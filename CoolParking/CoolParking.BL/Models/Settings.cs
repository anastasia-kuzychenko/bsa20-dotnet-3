﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

// У програмі мають бути передбачені Налаштування, чудово підійде статичний клас, у якому централізовано 
// знаходяться глобальні змінні та конфігурації, необхідні для роботи програми. У Налаштуваннях потрібно вказати:
//   Початковий баланс Паркінгу - 0;
//   Місткість Паркінгу - 10;
//   Період списання оплати, N-секунд - 5;
//   Період запису у лог, N-секунд - 60;
//   Тарифи в залежності від Тр.засобу: Легкова — 2, Вантажна — 5, Автобус — 3.5, Мотоцикл — 1;
//   Коефіцієнт штрафу - 2.5.


using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly object fileLocker; // для блогирование записи в logPath в 
        public static readonly int initialBalanceOfParking = 0;
        public static readonly int parkingCapacity = 10;
        public static readonly int withdrawTime = 5000;
        public static readonly int logTime = 60000;
        public static readonly decimal coefOfTheFine = 2.5M;
        public static readonly Dictionary<VehicleType, decimal> tariffs;

        static Settings()
        {
            fileLocker = new object();
            tariffs = new Dictionary<VehicleType, decimal>();
            tariffs.Add(VehicleType.PassengerCar, 2M);
            tariffs.Add(VehicleType.Truck, 5M);
            tariffs.Add(VehicleType.Bus, 3.5M);
            tariffs.Add(VehicleType.Motorcycle, 1M);
        }

    }
}